<?php
$title       = "Extensão de Cílios Fio a Fio em Cajamar";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A técnica de cílios fio a fio é um procedimento que, em vez de agrupar, separe os fios de seda e cole-os diretamente nos cílios a 0,2 cm de distância da pele, as clientes devem ter cílios saudáveis, nem muito jovens nem em fim de vida, assim, este procedimento será realizado da forma correta Este procedimento não é adequado para crianças menores de 16 anos porque os fios não suportam o peso da extensão.</p>
<p>Com uma ampla atuação no segmento, a Maxicilios oferece o melhor quando falamos de Extensão de Cílios Fio a Fio em Cajamar proporcionando aos seus clientes a máxima qualidade e desempenho em Micropigmentação de Sobrancelha Preço, Cílios Efeito Boneca Preço, Manutenção de Micropigmentação, Limpeza de Pele Profissional Preço e Designer de Sobrancelhas preço, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de cilios.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>