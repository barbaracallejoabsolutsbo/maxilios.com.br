<?php
$title       = "Manutenção de Cílios Russo em São Mateus";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Manutenção de Cílios Russo em São Mateus  é uma técnica de colocação de cílios de linha sintética muito popular nos últimos anos. Os fios russos nos cílios são adequados para mulheres que querem enfatizar sua aparência. Ligue para nossa equipe imediatamente e  marque um horário com a melhor empresa do mercado, a Maxicilios está sempre pronta para atender todos os clientes com excelente qualidade.</p>
<p>Com a Maxicilios proporcionando o que se tem de melhor e mais moderno no segmento de cilios consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Alongamento de Cílios Fio A Fio, Sobrancelha Fio a Fio Microblading, Sobrancelha Micropigmentada Preço, Limpeza de Pele Profunda Valor e Alongamento de Cílios Postiços Valor nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Manutenção de Cílios Russo em São Mateus.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>