<?php
$title       = "Alongamento de Cílios Fio A Fio no Jardim Paulistano";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Alongamento de Cílios Fio A Fio no Jardim Paulistano é  fios de seda separados e fixados diretamente nos cílios a 0,2 cm da pele. Os cílios saudáveis não devem ser muito jovens ou até o fim da vida. Para que você possa esticar. Este procedimento não é adequado para crianças menores de 16 anos porque os fios não suportam o peso da extensão, procure sempre a melhor para realizar este procedimento.</p>
<p>Líder no segmento de cilios, a Maxicilios dispõe dos melhores e mais modernos recursos do mercado. Nossa empresa trabalha com o objetivo de viabilizar Alongamento de Cílios Fio A Fio no Jardim Paulistano com a qualidade que você tanto procura. Somos, também, especializados em limpeza peeling de diamante, Sobrancelha de Henna Definitiva, Micropigmentação Fio a Fio, Depilação Intima Feminina Valor e Sobrancelha Micropigmentada Preço, pois, contamos com uma equipe competente e comprometida em prestar um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>