<?php
$title       = "Micropigmentação de Sobrancelhas Fio a Fio em Arujá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na técnica de micropigmentação de sobrancelha fio a fio, a espessura e a direção dos pelos são as mesmas da região que já existe na pele da cliente. Além disso, o pigmento é implantado na camada superficial da pele para deixar o resultado muito natural. Esta técnica é indicada para pessoas que possuem sobrancelhas franzidas e pode até trazer um efeito natural ao rosto, a Maxicilios oferece este procedimento de excelente qualidade.</p>
<p>Como uma empresa especializada em cilios proporcionamos sempre o melhor quando falamos de Alongamento de Cílios Postiços Valor, Depilação Corpo Todo Valor, Manutenção de Cílios Postiços, Design de Sobrancelha Com Henna e Cílios Postiço Naturais Preço. Com potencial necessário para garantir qualidade e excelência em Micropigmentação de Sobrancelhas Fio a Fio em Arujá com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Maxicilios trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>