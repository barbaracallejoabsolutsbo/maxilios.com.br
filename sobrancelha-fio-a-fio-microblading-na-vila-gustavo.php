<?php
$title       = "Sobrancelha Fio a Fio Microblading na Vila Gustavo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na realização da Sobrancelha Fio a Fio Microblading na Vila Gustavo, as restrições são as mesmas para depilação ou tatuagem, ou seja, evitar a exposição ao sol de alguns dias atrás, não riscar a área e evitar o uso de produtos que contenham ácidos. Após realizar este procedimento, os clientes podem recuperar as atividades diárias normais. Entre em contato com nossa equipe e saiba mais sobre esta técnica que realizamos.</p>
<p>Como líder do segmento de cilios, a Maxicilios se dispõe a oferecer uma excelente assessoria nas questões de Micropigmentação de Sobrancelha Preço, Estúdio de Sobrancelha de Rena, limpeza peeling de diamante, Extensão de Cílios Fio a Fio e Sobrancelha de Hena Preço sempre com muita qualidade e dedicação aos seus clientes e parceiros. Por contarmos com uma equipe profissionalmente competente para proporcionar o melhor em Sobrancelha Fio a Fio Microblading na Vila Gustavo ganhamos a confiança e preferência de nossos clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>