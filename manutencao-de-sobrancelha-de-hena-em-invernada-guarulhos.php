<?php
$title       = "Manutenção de Sobrancelha de Hena em Invernada - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A manutenção de sobrancelha de henna é uma técnica usada para preencher a mesma área dos fios. Portanto, esse procedimento é indicado para pessoas com sobrancelhas defeituosas, ou que desejam mudar ou aprimorar o design, ou apenas aumentar o volume desses fios, tornando-se uma técnica muito popular para as mulheres, sendo assim, procure uma empresa renomada para realizar esta técnica.</p>
<p>Você procura por Manutenção de Sobrancelha de Hena em Invernada - Guarulhos? Contar com empresas especializadas no segmento de cilios é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Maxicilios é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Extensão de Cílios Fio a Fio, Manutenção de Cílios Fio a Fio, Sobrancelha Fio a Fio Microblading, Manutenção de Cílios Postiços e Estúdio de Sobrancelha de Rena e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>