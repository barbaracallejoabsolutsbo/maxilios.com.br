<?php
$title       = "Alongamento de Cílios Efeito Natural em Jardim Tranquilidade - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A técnica de Alongamento de Cílios Efeito Natural em Jardim Tranquilidade - Guarulhos conquistou a preferência de muitas pessoas que desejam ter cílios longos e grossos sem o uso de cílios postiços, máscara ou modelador de cachos. Esta técnica é chamada de efeito natural e é o efeito mais natural. Isso porque apenas um fio sintético é adicionado a cada cílio natural, o que nada mais é do que adicionar um fio sintético e grudar em cada cílio natural.</p>
<p>Com credibilidade no mercado de cilios, a Maxicilios trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Sobrancelha de Hena Preço, Alongamento de Cílios Preço, Massagem Relaxante Preço, Sobrancelha Definitiva Fio a Fio e Depilação Corpo Todo Valor quanto em Alongamento de Cílios Efeito Natural em Jardim Tranquilidade - Guarulhos. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>