<?php
$title       = "Tatuagem de Henna Para Sobrancelha na Vila Mariana";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para fazer a Tatuagem de Henna Para Sobrancelha na Vila Mariana de acordo com seu tipo de pele, formato e rosto, escolha uma empresa renomada, assim, você terá a garantia de um procedimento de excelente qualidade,  a Maxiclios está sempre pronta para atender, entre em contato e marque um horário com a melhor empresa desse segmento, nossos profissionais são altamente qualificados para isso.</p>
<p>Como uma empresa de confiança no mercado de cilios, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Tatuagem de Henna Para Sobrancelha na Vila Mariana. A Maxicilios vem crescendo e mostrando seu potencial através de Tatuagem de Henna Para Sobrancelha, Manutenção de Cílios Russo, Extensão de Cílios Volume Russo, Micropigmentação Fio a Fio e Manutenção de Cílios Fio a Fio, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>