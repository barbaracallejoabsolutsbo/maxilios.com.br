<?php
$title       = "Sobrancelha Fio a Fio e Tatuagem na Vila Andrade";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A diferença entre Sobrancelha Fio a Fio e Tatuagem na Vila Andrade é que a camada da pele para a técnica da sobrancelha de fio a fio é "raspada" para o procedimento, enquanto a tatuagem cobre mais de uma camada, e as sobrancelhas permanentes alcançam apenas a primeira camada, sendo assim, procure uma empresa especializada para realizar esses dois procedimentos de excelente qualidade.</p>
<p>Sendo uma das principais empresas do segmento de cilios, a Maxicilios possui os melhores recursos do mercado com o objetivo de disponibilizar Sobrancelha Fio a Fio Microblading, Alongamento de Cílios Fio A Fio, Manutenção de Cílios Fio a Fio, Micropigmentação de Sobrancelhas Fio a Fio e Manutenção de Cílios Postiços com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Sobrancelha Fio a Fio e Tatuagem na Vila Andrade com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>