<?php
$title       = "Extração de Cravos e Espinhas em Ribeirão Pires";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A técnica de Extração de Cravos e Espinhas em Ribeirão Pires pode ser manual, com a pressão do dedo indicador e com algodão ou gaze embebida em desinfetante. No entanto, para eliminar os cravos e espinhas, devem ser utilizadas microagulhas que são utilizadas por profissionais,  para mais informações e agendamento, entre em contato conosco imediatamente. Estamos esperando por você para realizar um procedimento de alta qualidade. </p>
<p>Líder no segmento de cilios, a Maxicilios dispõe dos melhores e mais modernos recursos do mercado. Nossa empresa trabalha com o objetivo de viabilizar Extração de Cravos e Espinhas em Ribeirão Pires com a qualidade que você tanto procura. Somos, também, especializados em Sobrancelha de Hena Preço, Alongamento de Cílios Preço, Alongamento de Cílios Volume Russo, Cílios Postiço Naturais Preço e Limpeza de Pele Profunda Valor, pois, contamos com uma equipe competente e comprometida em prestar um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>