<?php
$title       = "Estúdio de Sobrancelha de Rena em Água Branca";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Consulte sempre os nossos profissionais do Estúdio de Sobrancelha de Rena em Água Branca que irão orientá-lo sobre a forma mais adequada ao seu rosto, respeitar a anatomia, o contorno e a harmonia do rosto, valorizar a aparência, respeitar a personalidade de cada um e as proporções corporais. O designer de sobrancelhas analisará tudo para determinar qual efeito é melhor para o rosto, realizando um procedimento de excelente qualidade.</p>
<p>Especialista no segmento de cilios, a Maxicilios é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Estúdio de Sobrancelha de Rena em Água Branca. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Manutenção de Cílios Postiços, Sobrancelha Fio a Fio e Tatuagem, Micropigmentação Fio a Fio, Extensão de Cílios Preço e Espaço Para Sobrancelhas e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>