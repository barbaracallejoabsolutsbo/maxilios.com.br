<?php
$title       = "Manutenção de Cílios Fio a Fio no Jardim Ângela";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A manutenção dos cílios fio a fio é uma técnica que separa os fios e os cola diretamente nos cílios a 0,2 cm da pele, para isso a cliente deve ter cílios saudáveis, nem muito jovens nem para o fim da vida. Para que você possa se alongar. Este procedimento não é adequado para crianças menores de 16 anos porque os fios não suportam o peso da extensão, entre em contato e marque seu horário com a melhor do mercado.</p>
<p>Especialista no mercado, a Maxicilios é uma empresa que ganha visibilidade quando se trata de Manutenção de Cílios Fio a Fio no Jardim Ângela, já que possui mão de obra especializada em Micropigmentação de Sobrancelhas Fio a Fio, Design de Sobrancelha Com Henna, Designer de Sobrancelhas preço, Limpeza de Pele Profunda Valor e Extensão de Cílios Fio a Fio. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de cilios, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>