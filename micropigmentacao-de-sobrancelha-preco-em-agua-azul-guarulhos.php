<?php
$title       = "Micropigmentação de Sobrancelha Preço em Água Azul - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na técnica de Micropigmentação de Sobrancelha Preço em Água Azul - Guarulhos que cabe no bolso, é utilizado um dispositivo denominado demógrafo, geralmente para trabalhos sensíveis e delicados, e uma agulha feita especialmente para esse fim é utilizada para aplicar o pigmento. Ao procurar um centro especializado em micropigmentação, você receberá um serviço profissional, portanto, entre em contato conosco imediatamente.</p>
<p>Por ser a principal empresa quando falamos de cilios, a Maxicilios se dispõe a adquirir os melhores e mais modernos recursos para atender seus clientes sempre da melhor forma. Possuindo como objetivo viabilizar tanto Extensão de Cílios Fio a Fio, Cílios Efeito Natural Preço, Depilação Com Cera Cavada, Sobrancelha Fio a Fio e Tatuagem e Cílios Postiço Naturais Preço, quanto como Micropigmentação de Sobrancelha Preço em Água Azul - Guarulhos mantendo a qualidade e a eficiência que você deseja. Entre em contato e faça uma cotação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>