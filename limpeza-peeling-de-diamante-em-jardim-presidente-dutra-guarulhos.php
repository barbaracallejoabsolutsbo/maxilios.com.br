<?php
$title       = "limpeza peeling de diamante em Jardim Presidente Dutra - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A limpeza de peeling de diamante não causa danos. Você pode retomar as atividades laborais e sociais imediatamente após a operação. Isso é diferente do que acontece quando você faz peelings químicos. Você precisa se afastar dessas atividades por alguns dias. Entre em contato agora para saber mais sobre nossos serviços. Estamos esperando por você, nossos procedimentos são de excelente qualidade.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em limpeza peeling de diamante em Jardim Presidente Dutra - Guarulhos? A Maxicilios é o que você precisa! Sendo uma das principais empresas do ramo de cilios consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Alongamento de Cílios Fio A Fio, Manutenção de Sobrancelha de Hena, Depilação Com Cera Cavada, Extração de Cravos e Espinhas e Sobrancelha Definitiva Fio a Fio. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>