<?php

    // Principais Dados do Cliente
$nome_empresa = "Maxílios";
$emailContato = "contato@maxilios.com.br";

    // Parâmetros de Unidade
$unidades = array(
    1 => array(
        "nome" => "Maxílios",
        "rua" => "Av. Jamaris, 100",
        "bairro" => "Moema",
        "cidade" => "São Paulo",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "04078-000",
        "latitude_longitude" => "", // Consultar no maps.google.com
        "ddd" => "11",
        "telefone" => "94575-7503",
        "telefone2" => "9999-9999",
        "whatsapp" => "94575-7503",
        "link_maps" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.9717609496815!2d-46.66393018502143!3d-23.605345684661053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5b9a040f7d19%3A0xac90e28c85fc889!2sAv.%20Jamaris%2C%20100%20-%20Moema%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004078-000!5e0!3m2!1spt-BR!2sbr!4v1625167815600!5m2!1spt-BR!2sbr" // Incorporar link do maps.google.com
    ),

    2 => array(
        "nome" => "",
        "rua" => "",
        "bairro" => "",
        "cidade" => "",
        "estado" => "",
        "uf" => "",
        "cep" => "",
        "ddd" => "",
        "telefone" => ""
    )
);

    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/maxilios.com.br/",
        // URL online
    "https://www.maxilios.com.br/"
));

    // Variáveis da head.php
$url = $padrao->url;
$canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );

    // Listas de Palavras Chave
    $palavras_chave = array(
        "limpeza peeling de diamante",
        "Alongamento de Cílios Efeito Natural",
        "Alongamento de Cílios Fio A Fio",
        "Alongamento de Cílios Postiços Valor",
        "Alongamento de Cílios Preço",
        "Alongamento de Cílios Volume Russo",
        "Cílios Efeito Boneca Preço",
        "Cílios Efeito Natural Preço",
        "Cílios Postiço Naturais Preço",
        "Clínica de Massagem Relaxante",
        "Design de Sobrancelha Com Henna",
        "Designer de Sobrancelhas preço",
        "Espaço Para Sobrancelhas",
        "Estúdio de Sobrancelha de Rena",
        "Extensão de Cílios Fio a Fio",
        "Extensão de Cílios Preço",
        "Extensão de Cílios Volume Russo",
        "Extração de Cravos e Espinhas",
        "Limpeza de Pele Profissional Preço",
        "Limpeza de Pele Profunda Valor",
        "Manutenção de Cílios Fio a Fio",
        "Manutenção de Cílios Postiços",
        "Manutenção de Cílios Russo",
        "Manutenção de Micropigmentação",
        "Manutenção de Sobrancelha de Hena",
        "Micropigmentação de Sobrancelha Preço",
        "Micropigmentação de Sobrancelhas Fio a Fio",
        "Micropigmentação Fio a Fio",
        "Sobrancelha de Hena Preço",
        "Sobrancelha de Henna Definitiva",
        "Sobrancelha Definitiva Fio a Fio",
        "Sobrancelha Fio a Fio e Tatuagem",
        "Sobrancelha Fio a Fio Microblading",
        "Sobrancelha Micropigmentada Preço",
        "Tatuagem de Henna Para Sobrancelha"
    );

    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */