<section class="veja-tambem">
    <h5>Veja Também</h5>
    <div class="row">
        <?php 
            echo $padrao->listaThumbs(
                $palavras_chave, array(
                    "random" => true,
                    "limit" => 6,
                    "class_div" => "col-xs-6 col-sm-2 col-md-2 col-lg-2",
                    "title_tag" => "h6"
                )
            );
        ?>
    </div>
</section>