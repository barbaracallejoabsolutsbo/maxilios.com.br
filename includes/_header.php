<header itemscope itemtype="http://schema.org/Organization">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NTM4D4Q"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	


    <div class="container header-container-main">
        <div class="logo-info-contato">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                    <div class="logo">
                        <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                            <span itemprop="image">
                                <img style="width: 130px;" src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="contato-header">
                        <div class="icone">
                            <img src="<?php echo $url; ?>imagens/icons/telefone.png" alt="Telefone" title="Telefone" class="img-responsive">
                            <p>
                                <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span></a>  <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone2"]; ?>"></a>
                                <br>
                                <a title="whatsApp" href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">
                                <span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a>
                                <br>
                                <a title="E-mail" href="mailto:<?php echo $emailContato; ?>?CC=<?php echo $emailAbsolut; ?>&Subject=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informações%20-%20<?php echo $nome_empresa; ?>">
                                    <span itemprop="telephone"> <?php echo $emailContato; ?></span>
                                </a> 
                            </p> 
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="contato-header">
                        <div class="icone">
                            <img src="<?php echo $url; ?>imagens/icons/mapa.png" alt="Telefone" title="Telefone" class="img-responsive">
                            <p><?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"]; ?></p>
                            <p>R. Voluntários da Pátria, 2525 - CJ 161 - Santana</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <nav class="menu">
                    <ul class="menu-list">
                        <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                        <!-- <li><a href="<?php echo $url; ?>empresa" title="Sobre a Empresa">Sobre a Empresa</a></li>
                        <li><a href="<?php echo $url; ?>servicos" title="Serviços">Serviços</a> -->
                            <!-- <ul class="sub-menu">
                               <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                               <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                               <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                            </ul> -->
                        </li>
                        <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                        <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>