<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="logo text-justify">
                    <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                        <span itemprop="image">
                            <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                        </span>
                    </a>
                    <p style="text-align: center; margin-bottom: 40px;">Chegando ao Brasil, em 2020, criou o espaço que hoje é conhecido como Maxílios e realiza alongamento de cílios usando as técnicas estudadas por anos no exterior. Usando somente materiais de altíssima qualidade, a Maxílios garante às clientes maior durabilidade e volume do alongamento!</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <h5>Atendimento:</h5>
                <p>Segunda à Sexta</p>
                <p>Das 9h00 às 19h00</p>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <h5>Contatos</h5>
                <p><strong>Telefone</strong>:<a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span></a></p>
                <p><strong>WhatsApp</strong>: <a title="whatsApp" href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">
                    <span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a></p>
                    <!-- <p><strong>Endereço</strong>: <?php echo $unidades[1]["rua"] . " " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"]; ?></p> -->
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <h5>Institucional</h5>
                    <ul>
                        <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                        <!-- <li><a href="<?php echo $url; ?>empresa" title="Sobre">Sobre</a></li> -->
                        <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                        <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                        <li><a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <h5>Endereço</h5>
                    <p><?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"] . " - " . $unidades[1]["cep"]; ?></p>
                    <p>R. Voluntários da Pátria, 2525 - CJ 161 - Santana, São Paulo - SP, 02401-000</p>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-left">
                        <p class="desenvolvido">Copyright © 2021 <?php echo $nome_empresa; ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                        <div class="baixa-absolut"></div>
                        <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                            <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap">
                        </a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"></a></li>
            <li><a href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es." class="mm-whatsapp" title="Whats App"></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>?CC=<?php echo $emailAbsolut; ?>&Subject=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informações%20-%20<?php echo $nome_empresa; ?>" class="mm-email" title="E-mail"></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"></button></li>
        </ul>
    </footer>
    <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-MRXDXF0PSW"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-MRXDXF0PSW');
      </script>
      <?php } ?>