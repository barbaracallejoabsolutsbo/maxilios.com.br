<?php
$title       = "Extensão de Cílios Preço no Jardim Ângela";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para as mulheres que querem melhorar sua aparência, a extensão de cílios de preço ótimo sempre foi uma boa escolha. O método inclui a aplicação de fio sintético ou fio de seda nos cílios naturais de cada cliente. Os materiais e métodos de aplicação utilizados não prejudicam a saúde do fio natural. Entre em contato com nossa equipe e saiba mais sobre os procedimentos que realizamos com excelente qualidade.</p>
<p>A Maxicilios, além de ser especializados em Limpeza de Pele Profunda Valor, Manutenção de Cílios Fio a Fio, Estúdio de Sobrancelha de Rena, Extração de Cravos e Espinhas e Sobrancelha Fio a Fio Microblading, atuando no mercado de cilios com a missão de oferecer sempre o melhor para seus clientes, de forma que seus objetivos sejam alcançados. Além disso, contamos com profissionais competentes visando prestar o melhor atendimento possível em Extensão de Cílios Preço no Jardim Ângela para ser sempre a opção número um de nossos parceiros e clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>