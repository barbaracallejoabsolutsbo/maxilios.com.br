<?php
$title       = "limpeza peeling de diamante em São Bernardo do Campo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A limpeza de peeling de diamante não causa danos. Você pode retomar as atividades laborais e sociais imediatamente após a operação. Isso é diferente do que acontece quando você faz peelings químicos. Você precisa se afastar dessas atividades por alguns dias. Entre em contato agora para saber mais sobre nossos serviços. Estamos esperando por você, nossos procedimentos são de excelente qualidade.</p>
<p>Especialista no mercado, a Maxicilios é uma empresa que ganha visibilidade quando se trata de limpeza peeling de diamante em São Bernardo do Campo, já que possui mão de obra especializada em Micropigmentação de Sobrancelha Preço, Estúdio de Sobrancelha de Rena, Manutenção de Sobrancelha de Hena, Sobrancelha Definitiva Fio a Fio e Extensão de Cílios Fio a Fio. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de cilios, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>