<?php
$title       = "Estúdio de Sobrancelha de Rena no Parque do Carmo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Consulte sempre os nossos profissionais do Estúdio de Sobrancelha de Rena no Parque do Carmo que irão orientá-lo sobre a forma mais adequada ao seu rosto, respeitar a anatomia, o contorno e a harmonia do rosto, valorizar a aparência, respeitar a personalidade de cada um e as proporções corporais. O designer de sobrancelhas analisará tudo para determinar qual efeito é melhor para o rosto, realizando um procedimento de excelente qualidade.</p>
<p>Como líder do segmento de cilios, a Maxicilios se dispõe a oferecer uma excelente assessoria nas questões de Estúdio de Sobrancelha de Rena, Sobrancelha Fio a Fio Microblading, Alongamento de Cílios Preço, Manutenção de Micropigmentação e Alongamento de Cílios Volume Russo sempre com muita qualidade e dedicação aos seus clientes e parceiros. Por contarmos com uma equipe profissionalmente competente para proporcionar o melhor em Estúdio de Sobrancelha de Rena no Parque do Carmo ganhamos a confiança e preferência de nossos clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>