<?php
$title       = "Espaço Para Sobrancelhas em Itaim Bibi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Busque sempre a opinião dos profissionais que atuam neste segmento, eles vão te orientar sobre qual formato é mais adequado para o seu rosto, respeitar a anatomia, aparência e harmonia, valorizar a aparência, respeitar a personalidade de cada pessoa e as proporções corporais. Tudo para determinar qual efeito é melhor para o rosto, por isso, conte sempre com o melhor Espaço Para Sobrancelhas em Itaim Bibi.</p>
<p>Tendo como especialidade Designer de Sobrancelhas preço, Limpeza de Pele Profunda Valor, Micropigmentação de Sobrancelha Preço, Extração de Cravos e Espinhas e Sobrancelha Fio a Fio e Tatuagem, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de cilios. Por isso, quando falamos de Espaço Para Sobrancelhas em Itaim Bibi, buscar pelos membros da empresa Maxicilios é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>