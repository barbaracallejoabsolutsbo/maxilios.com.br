<?php
$title       = "Manutenção de Cílios Postiços em Água Branca";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para realizar a Manutenção de Cílios Postiços em Água Branca o profissional deve primeiro esterilizar seus cílios, em seguida, colar a fita na pálpebra inferior e desenhar o desenho de cílios desejado na pálpebra inferior. Então, a própria colagem começou. Ligue para nossa equipe hoje e marque um horário com a melhor empresa do mercado, estamos sempre prontos para atender você com excelente qualidade.</p>
<p>Na busca por uma empresa referência, quando o assunto é cilios, a Maxicilios será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Alongamento de Cílios Efeito Natural, Alongamento de Cílios Postiços Valor, Alongamento de Cílios Preço, Depilação Corpo Todo Valor e Massagem Relaxante Preço, oferece Manutenção de Cílios Postiços em Água Branca com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>