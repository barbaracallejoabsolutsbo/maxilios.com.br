<?php
$title       = "Sobrancelha Fio a Fio e Tatuagem em Monte Carmelo - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A diferença entre Sobrancelha Fio a Fio e Tatuagem em Monte Carmelo - Guarulhos é que a camada da pele para a técnica da sobrancelha de fio a fio é "raspada" para o procedimento, enquanto a tatuagem cobre mais de uma camada, e as sobrancelhas permanentes alcançam apenas a primeira camada, sendo assim, procure uma empresa especializada para realizar esses dois procedimentos de excelente qualidade.</p>
<p>Com a Maxicilios proporcionando de forma excelente Sobrancelha de Hena Preço, Manutenção de Cílios Fio a Fio, Sobrancelha Micropigmentada Preço, Micropigmentação de Sobrancelhas Fio a Fio e Alongamento de Cílios Preço conseguindo manter a alta qualidade e credibilidade no ramo de cilios, assim, consequentemente, proporcionando o que se tem de melhor em resultados para você. Possibilitando diversas escolhas para os melhores resultados, a nossa empresa torna-se referência com Sobrancelha Fio a Fio e Tatuagem em Monte Carmelo - Guarulhos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>