<?php
$title       = "Manutenção de Micropigmentação na Ponte Rasa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao realizar a manutenção do micropigmentação as restrições são as mesmas da depilação ou tatuagem, ou seja, nos primeiros dias após o processamento da micropigmentação, evite a exposição ao sol, não arranhe a área e evite o uso de produtos que contenham ácido. Os clientes podem retomar as atividades diárias normais. Entre em contato com nossa equipe e saiba mais informações sobre este procedimento.</p>
<p>A empresa Maxicilios é destaque entre as principais empresas do ramo de cilios, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Manutenção de Micropigmentação na Ponte Rasa do mercado. Ainda, possui facilidade com Manutenção de Micropigmentação, Cílios Efeito Natural Preço, Manutenção de Sobrancelha de Hena, Micropigmentação Fio a Fio e Alongamento de Cílios Postiços Valor mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>