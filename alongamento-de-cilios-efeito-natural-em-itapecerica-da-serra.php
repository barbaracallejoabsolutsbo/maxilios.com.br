<?php
$title       = "Alongamento de Cílios Efeito Natural em Itapecerica da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A técnica de Alongamento de Cílios Efeito Natural em Itapecerica da Serra conquistou a preferência de muitas pessoas que desejam ter cílios longos e grossos sem o uso de cílios postiços, máscara ou modelador de cachos. Esta técnica é chamada de efeito natural e é o efeito mais natural. Isso porque apenas um fio sintético é adicionado a cada cílio natural, o que nada mais é do que adicionar um fio sintético e grudar em cada cílio natural.</p>
<p>Especialista no segmento de cilios, a Maxicilios é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Alongamento de Cílios Efeito Natural em Itapecerica da Serra. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Manutenção de Cílios Russo, Manutenção de Sobrancelha de Hena, limpeza peeling de diamante, Sobrancelha Micropigmentada Preço e Massagem Relaxante Preço e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>