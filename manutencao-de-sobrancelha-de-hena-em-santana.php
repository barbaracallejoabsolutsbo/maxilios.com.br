<?php
$title       = "Manutenção de Sobrancelha de Hena em Santana";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A manutenção de sobrancelha de henna é uma técnica usada para preencher a mesma área dos fios. Portanto, esse procedimento é indicado para pessoas com sobrancelhas defeituosas, ou que desejam mudar ou aprimorar o design, ou apenas aumentar o volume desses fios, tornando-se uma técnica muito popular para as mulheres, sendo assim, procure uma empresa renomada para realizar esta técnica.</p>
<p>Desempenhando uma das melhores assessorias do segmento de cilios, a Maxicilios se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Manutenção de Sobrancelha de Hena em Santana com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Manutenção de Sobrancelha de Hena, Sobrancelha Micropigmentada Preço, Alongamento de Cílios Preço, Manutenção de Cílios Fio a Fio e Design de Sobrancelha Com Henna, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>