<?php
$title       = "Micropigmentação de Sobrancelhas Fio a Fio na Bela Vista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao realizar a técnica de micropigmentação da sobrancelha fio a fio, as restrições são as mesmas para depilação ou tatuagem, ou seja, evitar a exposição ao sol nos primeiros dias, não riscar a região e evitar o uso de produtos que contenham ácido. Depois de realizar este procedimento de micropigmentação, os clientes podem retomar as atividades diárias normais, procure sempre profissionais qualificados para realizar esta técnica.</p>
<p>Buscando por uma empresa de credibilidade no segmento de cilios, para que, você que busca por Micropigmentação de Sobrancelhas Fio a Fio na Bela Vista, tenha a garantia de qualidade e idoneidade, contar com a Maxicilios é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Sobrancelha Fio a Fio e Tatuagem, Depilação Intima Feminina Valor, Design de Sobrancelha Com Henna, Depilação Com Cera Cavada e Cílios Postiço Naturais Preço para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>