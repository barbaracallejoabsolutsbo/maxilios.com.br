<?php
$title       = "Sobrancelha de Henna Definitiva na Vila Maria";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As sobrancelhas de henna definitiva usam um corante, neste caso em particular, pode corrigir manchas e criar um desenho de contorno assimétrico nas sobrancelhas, você pode escolher a cor que será usada, não só existe preto no mercado de sobrancelhas, também tem marrom, tons de marrom claro e dourado escuro, dependendo do tom da sua pele e da cor do cabelo, faça esta técnica com a melhor.</p>
<p>Especialista no segmento de cilios, a Maxicilios é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Sobrancelha de Henna Definitiva na Vila Maria. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Alongamento de Cílios Volume Russo, Micropigmentação de Sobrancelha Preço, Sobrancelha de Henna Definitiva, Sobrancelha Fio a Fio Microblading e Cílios Efeito Boneca Preço e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>