<?php
$title       = "Estúdio de Sobrancelha de Rena em Porto da Igreja - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Quando as sobrancelhas não estão em harmonia com outros elementos faciais as sobrancelhas têm um impacto estético negativo, somos um estúdio de sobrancelhas de rena composto por profissionais de salão de beleza que irão avaliar o rosto e os elementos faciais para criar harmonia entre todos os elementos. Entre em contato imediatamente e marque seu horário com a melhor do ramo.</p>
<p>A Maxicilios, como uma empresa em constante desenvolvimento quando se trata do mercado de cilios visa trazer o melhor resultado em Estúdio de Sobrancelha de Rena em Porto da Igreja - Guarulhos para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Cílios Postiço Naturais Preço, Clínica de Massagem Relaxante, Sobrancelha de Hena Preço, Manutenção de Cílios Fio a Fio e Manutenção de Sobrancelha de Hena para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>