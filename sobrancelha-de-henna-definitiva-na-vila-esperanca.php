<?php
$title       = "Sobrancelha de Henna Definitiva na Vila Esperança";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A diferença entre tatuagens e Sobrancelha de Henna Definitiva na Vila Esperança é que algumas camadas de pele são "raspadas" para o procedimento, enquanto as tatuagens atingem mais de uma camada e as sobrancelhas permanentes e alcançam apenas a primeira camada. Seja muito claro e sincero com os profissionais e diga o que você está procurando, assim, o resultado final será o que você deseja.</p>
<p>A empresa Maxicilios é destaque entre as principais empresas do ramo de cilios, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Sobrancelha de Henna Definitiva na Vila Esperança do mercado. Ainda, possui facilidade com Micropigmentação de Sobrancelhas Fio a Fio, Sobrancelha Definitiva Fio a Fio, Manutenção de Micropigmentação, Alongamento de Cílios Fio A Fio e Manutenção de Cílios Postiços mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>