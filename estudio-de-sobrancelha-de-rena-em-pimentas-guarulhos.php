<?php
$title       = "Estúdio de Sobrancelha de Rena em Pimentas - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Consulte sempre os nossos profissionais do Estúdio de Sobrancelha de Rena em Pimentas - Guarulhos que irão orientá-lo sobre a forma mais adequada ao seu rosto, respeitar a anatomia, o contorno e a harmonia do rosto, valorizar a aparência, respeitar a personalidade de cada um e as proporções corporais. O designer de sobrancelhas analisará tudo para determinar qual efeito é melhor para o rosto, realizando um procedimento de excelente qualidade.</p>
<p>Com credibilidade no mercado de cilios, a Maxicilios trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Limpeza de Pele Profissional Preço, Clínica de Massagem Relaxante, Sobrancelha de Henna Definitiva, Sobrancelha Fio a Fio Microblading e Alongamento de Cílios Volume Russo quanto em Estúdio de Sobrancelha de Rena em Pimentas - Guarulhos. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>