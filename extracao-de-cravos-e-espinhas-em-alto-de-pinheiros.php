<?php
$title       = "Extração de Cravos e Espinhas em Alto de Pinheiros";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A maneira mais segura de realizar a extração de cravo e espinhas é consultar um dermatologista ou um profissional qualificado. O ideal é que a pele seja preparada e as substâncias utilizadas pelos profissionais possam amenizar as secreções na área de avaliação e dilatar os poros por meio do vapor, por isso, procure uma empresa especializada neste segmento, assim, você terá um procedimento de alto nível.</p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de cilios, a Maxicilios é uma empresa experiente quando se trata do ramo de Manutenção de Sobrancelha de Hena, Sobrancelha de Henna Definitiva, Manutenção de Cílios Russo, Depilação Corpo Todo Valor e Extensão de Cílios Fio a Fio. Por isso, se você busca o melhor com um custo acessível e vantajoso em Extração de Cravos e Espinhas em Alto de Pinheiros aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>