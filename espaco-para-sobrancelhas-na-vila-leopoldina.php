<?php
$title       = "Espaço Para Sobrancelhas na Vila Leopoldina";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Busque sempre a opinião dos profissionais que atuam neste segmento, eles vão te orientar sobre qual formato é mais adequado para o seu rosto, respeitar a anatomia, aparência e harmonia, valorizar a aparência, respeitar a personalidade de cada pessoa e as proporções corporais. Tudo para determinar qual efeito é melhor para o rosto, por isso, conte sempre com o melhor Espaço Para Sobrancelhas na Vila Leopoldina.</p>
<p>Você procura por Espaço Para Sobrancelhas na Vila Leopoldina? Contar com empresas especializadas no segmento de cilios é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Maxicilios é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Manutenção de Micropigmentação, Manutenção de Cílios Russo, Micropigmentação de Sobrancelha Preço, Cílios Efeito Natural Preço e Cílios Postiço Naturais Preço e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>