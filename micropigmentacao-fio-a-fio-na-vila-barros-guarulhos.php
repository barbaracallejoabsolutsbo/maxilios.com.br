<?php
$title       = "Micropigmentação Fio a Fio na Vila Barros - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao realizar a técnica de micropigmentação de fio a fio, as restrições são as mesmas para depilação ou tatuagem, ou seja, evite exposição ao sol nos primeiros dias, não arranhe a área e evite usar produtos que contenham ácido. Neste procedimento de micropigmentação, o cliente pode recuperar as atividades diárias normais. Entre em contato com nossa equipe para saber mais informações sobre esta técnica.</p>
<p>Como uma empresa especializada em cilios proporcionamos sempre o melhor quando falamos de Manutenção de Cílios Fio a Fio, Alongamento de Cílios Fio A Fio, Cílios Postiço Naturais Preço, Tatuagem de Henna Para Sobrancelha e Extração de Cravos e Espinhas. Com potencial necessário para garantir qualidade e excelência em Micropigmentação Fio a Fio na Vila Barros - Guarulhos com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Maxicilios trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>