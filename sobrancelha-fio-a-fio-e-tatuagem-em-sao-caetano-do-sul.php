<?php
$title       = "Sobrancelha Fio a Fio e Tatuagem em São Caetano do Sul";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A vida útil de Sobrancelha Fio a Fio e Tatuagem em São Caetano do Sul é de cerca de 1 a 5 anos, dependendo do tipo de pele de cada pessoa e da técnica que você escolher usar. Essa técnica é indicada para mulheres com poucas sobrancelhas e muitas manchas, ou mulheres que gostam de ser bem definidas. Entre em contato conosco agora e marque um horário com a melhor empresa deste segmento, não perca tempo.</p>
<p>Como uma especialista em cilios, a Maxicilios se destaca dentre as demais empresas quando o assunto é Sobrancelha Fio a Fio e Tatuagem em São Caetano do Sul, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Limpeza de Pele Profunda Valor, Manutenção de Micropigmentação, limpeza peeling de diamante, Limpeza de Pele Profissional Preço e Extensão de Cílios Fio a Fio. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>