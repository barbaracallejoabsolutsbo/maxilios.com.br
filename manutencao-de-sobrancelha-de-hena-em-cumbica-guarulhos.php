<?php
$title       = "Manutenção de Sobrancelha de Hena em Cumbica - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A manutenção de sobrancelha de henna é uma técnica muito comum entre as mulheres. O procedimento visa corrigir pequenas imperfeições na área, além de ajudar a definir um design mais harmonioso para o rosto feminino. A Henna atua na pele dando um efeito de sombra às sobrancelhas. Portanto, este procedimento deve ser realizado com profissionais que entende do assunto.</p>
<p>Se está procurando por Manutenção de Sobrancelha de Hena em Cumbica - Guarulhos e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Maxicilios é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de cilios conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Depilação Com Cera Cavada, Espaço Para Sobrancelhas, Alongamento de Cílios Efeito Natural, Sobrancelha Fio a Fio e Tatuagem e Manutenção de Cílios Russo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>