<?php
$title       = "Clínica de Massagem Relaxante em São Caetano do Sul";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Clínica de Massagem Relaxante em São Caetano do Sul oferece massagens que são benéficas para a flexibilidade e a circulação sanguínea e criam uma sensação de felicidade e alegria psicologicamente. A massagem relaxante também ajuda a relaxar o corpo e a mente. A massagem proporciona relaxamento profundo, reduzindo as dores musculares, aumentando e melhorando a oxigenação das células, marque um horário com a melhor.</p>
<p>Nós da Maxicilios trabalhamos dia a dia para garantir o melhor em Clínica de Massagem Relaxante em São Caetano do Sul e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de cilios com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Espaço Para Sobrancelhas, Tatuagem de Henna Para Sobrancelha, Micropigmentação de Sobrancelhas Fio a Fio, Design de Sobrancelha Com Henna e Sobrancelha Fio a Fio e Tatuagem e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>