<?php
$title       = "Micropigmentação de Sobrancelhas Fio a Fio em Invernada - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao realizar a técnica de micropigmentação da sobrancelha fio a fio, as restrições são as mesmas para depilação ou tatuagem, ou seja, evitar a exposição ao sol nos primeiros dias, não riscar a região e evitar o uso de produtos que contenham ácido. Depois de realizar este procedimento de micropigmentação, os clientes podem retomar as atividades diárias normais, procure sempre profissionais qualificados para realizar esta técnica.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de cilios, a Maxicilios oferece a confiança e a qualidade que você procura quando falamos de Alongamento de Cílios Fio A Fio, Alongamento de Cílios Postiços Valor, Manutenção de Cílios Russo, Sobrancelha Micropigmentada Preço e Extração de Cravos e Espinhas. Ainda, com o mais acessível custo x benefício para quem busca Micropigmentação de Sobrancelhas Fio a Fio em Invernada - Guarulhos, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>