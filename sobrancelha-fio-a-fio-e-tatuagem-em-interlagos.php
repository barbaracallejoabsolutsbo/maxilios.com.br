<?php
$title       = "Sobrancelha Fio a Fio e Tatuagem em Interlagos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A vida útil de Sobrancelha Fio a Fio e Tatuagem em Interlagos é de cerca de 1 a 5 anos, dependendo do tipo de pele de cada pessoa e da técnica que você escolher usar. Essa técnica é indicada para mulheres com poucas sobrancelhas e muitas manchas, ou mulheres que gostam de ser bem definidas. Entre em contato conosco agora e marque um horário com a melhor empresa deste segmento, não perca tempo.</p>
<p>Se deseja comprar Sobrancelha Fio a Fio e Tatuagem em Interlagos e procura por uma empresa séria e competente, a Maxicilios é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Manutenção de Cílios Russo, Manutenção de Micropigmentação, Micropigmentação de Sobrancelhas Fio a Fio, Cílios Efeito Natural Preço e Manutenção de Cílios Fio a Fio. Entre em contato e saiba tudo sobre cilios com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>