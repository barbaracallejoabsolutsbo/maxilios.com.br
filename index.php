<?php

$h1      	 = "Home";
$title    	 = "Alongamento de Cílios";
    $description = "Chegando ao Brasil, em 2020, criou o espaço que hoje é conhecido como Maxílios e realiza alongamento de cílios usando as técnicas estudadas por anos no exterior."; // Manter entre 130 a 160 caracteres
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "home"
    ));
    
    ?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="banner-home">
            <div class="theme-default-nivo-slider">
                <div id="slider" class="nivoSlider"> 
                    <img src="<?php echo $url; ?>imagens/fachada.jpg" alt="Imagem">
                </div>
            </div>
        </div>
        <div class="sessao-banner">
            <h1>Alongamento de Cílios<br>com Técnica de Longa Duração!</h1>
            <a class="btn-home" href="<?php echo $url?>">Saiba mais</a>
            <a class="btn-whatsapp-home" target="_blank" title="whatsApp" href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">
                <i class="fab fa-whatsapp"></i>(11) 94575-7503
            </a>
        </div>
        <div class="atendimento-home">
            <div class="container">
                <div class="row text-center">
                    <h2>Como Funciona o Alongamento de Cílios</h2>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <i class="fas fa-calendar-alt"></i>
                        <h3>Tempo para aplicação: </h3>
                        <p>1h-2h</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                     <i class="fas fa-calendar-alt"></i>
                     <h3>Duração do alongamento: </h3>
                     <p>Até 60 dias com apenas 1 manutenção</p>
                 </div>
                 <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <i class="fas fa-calendar-alt"></i>
                    <h3>Período de manutenção: </h3>
                    <p>20-30 dias</p>
                </div>

            </div>
        </div>
    </div>
    <div class="sobre-nos">
        <div class="container">
            <h2>Sobre a Maxílios Espaço Keissy Costa</h2>
            <hr>
            <p>Keissy Costa morou no Japão durante 18 anos, e durante esse período se aperfeiçoou na técnica de alongamento de cílios fio-a-fio usada para realçar o olhar sem perder sua naturalidade.</p>

            <p>Chegando ao Brasil, em 2020, criou o espaço que hoje é conhecido como Maxílios e realiza alongamento de cílios usando as técnicas estudadas por anos no exterior.</p>

            <p>Usando somente materiais de altíssima qualidade, a Maxílios garante às clientes maior durabilidade e volume do alongamento!</p>
            <br>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="content">
                        <a href="<?php echo $url; ?>" target="_blank">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/tipos/01.png" class="img-responsive content-image" alt="Imagem" title="">
                            <div class="content-details fadeIn-top">
                                <h3>Alongamento Clássico</h3>
                                <p>Para um efeito mais natural.</p>
                                <p>Em média, 200 fios</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="content">
                        <a href="<?php echo $url; ?>" target="_blank">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/tipos/02.png" class="img-responsive content-image" alt="Imagem" title="">
                            <div class="content-details fadeIn-top">
                                <h3>Alongamento W Max</h3>
                                <p>Para volume e durabilidade.</p>
                                <p>Em média, 280 fios</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="content">
                        <a href="<?php echo $url; ?>" target="_blank">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/tipos/03.png" class="img-responsive content-image" alt="Imagem" title="">
                            <div class="content-details fadeIn-top">
                                <h3>Alongamento Max Volume</h3>
                                <p>Para um olhar marcante e sensual.</p>
                                <p>Em média, 380 fios</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="contador-home">
        <div class="efeito">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3 class="counter">10</h3>
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3 class="counter">12</h3>
                        <p>Lorem Ipsums</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3 class="counter">3502</h3>
                        <p>Lorem Ipsum</p>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="informacoes-home">
        <div class="container">
            <h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
            <hr>
            <div class="lista-galeria-fancy row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <br>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <br>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="content">
                        <a href="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" title="" data-fancybox-group="item">
                            <div class="content-overlay"></div>
                            <img src="<?php echo $url; ?>imagens/nossos-servicos/sem-imagem.jpg" alt="Imagem" title="" class="img-responsive">
                            <div class="content-details fadeIn-top">
                                <h3>Lorem Ipsum</h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>  -->
    <div class="clientes">
        <div class="container">
          <div class="info">
            <img class="img-responsive" src="<?php echo $url; ?>imagens/info/01.png" alt="Imagem" title="Imagem">
            <p>Técnica Japonesa</p>
        </div>
        <div class="info">
            <img class="img-responsive" src="<?php echo $url; ?>imagens/info/02.png" alt="Imagem" title="Imagem">
            <p>Produtos Importados</p>
        </div>
        <div class="info">
            <img class="img-responsive" src="<?php echo $url; ?>imagens/info/03.png" alt="Imagem" title="Imagem">
            <p>Não Incomoda</p>
        </div>
        <div class="info">
            <img class="img-responsive" src="<?php echo $url; ?>imagens/info/04.png" alt="Imagem" title="Imagem">
            <p>Mais Durabilidade</p>
        </div>
        <div class="info">
            <img class="img-responsive" src="<?php echo $url; ?>imagens/info/05.png" alt="Imagem" title="Imagem">
            <p>Não Danifica os Cílios</p>
        </div>
    </div>
</div>

    

<div class="comentarios-google">
    <div class="container">
        <h2>Comentários do <b style="color:#4285f4;">G</b><b style="color:#ea4335;">O</b><b style="color:#fbbc05;">O</b><b style="color:#4285f4;">G</b><b style="color:#34a853;">L</b><b style="color:#ea4335;">E</b></h2>

        <div class="comentarios">
         <div class="autoplay">
            <div class="comentario">
                <h3>Sabrina Ferreira</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Limpeza, Pontualidade, Profissionalismo, Qualidade, Valor</p>
            </div>
            <div class="comentario">
                <h3>Daniela Cintra</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Amei, excelente trabalho, perfeito!!!!</p>
            </div>
            <div class="comentario">
                <h3>Vinicius Samios</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Excelente trabalho, satisfação garantida!</p>
            </div>
            <div class="comentario">
                <h3>Monica Moschetto Winther de Castro</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Limpeza, Pontualidade, Profissionalismo, Qualidade, Valor</p>
            </div>
            <div class="comentario">
                <h3>Lilian Silva</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Excelente!  Amei o resultado.</p>
            </div>
            <div class="comentario">
                <h3>Beth Sena</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Amei seu trabalho está de parabéns</p>
            </div>
            <div class="comentario">
                <h3>Iza maria Zamberlan</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Perfeita! Adoro!</p>
            </div>
            <div class="comentario">
                <h3>Sandra Morais</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ótimo trabalho!</p>
            </div>
            <div class="comentario">
                <h3>Feeh Sattler</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Limpeza, Pontualidade, Profissionalismo, Qualidade, Valor</p>
            </div>
            <div class="comentario">
                <h3>Evania Batista</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Adorei seu trabalho</p>
            </div>
            <div class="comentario">
                <h3>Gabriele Saw</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Profissionais bem qualificadas. Produto de ótima qualidade e ambiente confortável.</p>
            </div>
            <div class="comentario">
                <h3>Eliane Pinho</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ótimo atendimento, excelente profissional. Os cílios ficam bem naturais, fica lindo.</p>
            </div>
            <div class="comentario">
                <h3>Josy Carvalho</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Excelente profissional..trabalho de qualidade super recomendo...</p>
            </div>
            <div class="comentario">
                <h3>Rosilene Poffo</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ficou muito natural...meus cílios estao simplesmente Maravilhosos.</p>
            </div>
            <div class="comentario">
                <h3>Thais Natalia</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Super profissional só agradeço por ter encontrado ela 😍😍😍😍super indico de olhos fechados 😍</p>
            </div>
            <div class="comentario">
                <h3>Liana Alves Chiromatzo</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Adorei! Trabalho super bem feito e natural! Virei cliente ❤️</p>
            </div>
            <div class="comentario">
                <h3>Amanda Freitas</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ótimo atendimento, produto de boa qualidade. Não senti nada na colocação e fica lindo por bastante tempo!</p>
            </div>
            <div class="comentario">
                <h3>Frances karla Viana cardoso</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Adorei o serviço.  Fiz em outros lugares e nada comparava. Delicados  e não tem excessos  de cola muito natural</p>
            </div>
            <div class="comentario">
                <h3>Patricia Sakugawa</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>A Keissy é uma ótima profissional , faz seu trabalho , com carinho e atenção, lugar calmo , limpo e agradável , super recomendo</p>
            </div>
            <div class="comentario">
                <h3>Manami Takano</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Já estou fazendo meus cílios por 2 anos com ela! Super profissional com técnica japonesa!</p>
            </div>
        </div>

        <div class="comentarios">
         <div class="col-md-12">
            <div class="comentario">
                <h3>Sabrina Ferreira</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Limpeza, Pontualidade, Profissionalismo, Qualidade, Valor</p>
            </div>
            <div class="comentario">
                <h3>Daniela Cintra</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Amei, excelente trabalho, perfeito!!!!</p>
            </div>
            <div class="comentario">
                <h3>Vinicius Samios</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Excelente trabalho, satisfação garantida!</p>
            </div>
            <div class="comentario">
                <h3>Monica Moschetto Winther de Castro</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Limpeza, Pontualidade, Profissionalismo, Qualidade, Valor</p>
            </div>
            <div class="comentario">
                <h3>Lilian Silva</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Excelente!  Amei o resultado.</p>
            </div>
            <div class="comentario">
                <h3>Beth Sena</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Amei seu trabalho está de parabéns</p>
            </div>
            <div class="comentario">
                <h3>Iza maria Zamberlan</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Perfeita! Adoro!</p>
            </div>
            <div class="comentario">
                <h3>Sandra Morais</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ótimo trabalho!</p>
            </div>
            <div class="comentario">
                <h3>Feeh Sattler</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Limpeza, Pontualidade, Profissionalismo, Qualidade, Valor</p>
            </div>
            <div class="comentario">
                <h3>Evania Batista</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Adorei seu trabalho</p>
            </div>
            <div class="comentario">
                <h3>Gabriele Saw</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Profissionais bem qualificadas. Produto de ótima qualidade e ambiente confortável.</p>
            </div>
            <div class="comentario">
                <h3>Eliane Pinho</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ótimo atendimento, excelente profissional. Os cílios ficam bem naturais, fica lindo.</p>
            </div>
            <div class="comentario">
                <h3>Josy Carvalho</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Excelente profissional..trabalho de qualidade super recomendo...</p>
            </div>
            <div class="comentario">
                <h3>Rosilene Poffo</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ficou muito natural...meus cílios estao simplesmente Maravilhosos.</p>
            </div>
            <div class="comentario">
                <h3>Thais Natalia</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Super profissional só agradeço por ter encontrado ela 😍😍😍😍super indico de olhos fechados 😍</p>
            </div>
            <div class="comentario">
                <h3>Liana Alves Chiromatzo</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Adorei! Trabalho super bem feito e natural! Virei cliente ❤️</p>
            </div>
            <div class="comentario">
                <h3>Amanda Freitas</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Ótimo atendimento, produto de boa qualidade. Não senti nada na colocação e fica lindo por bastante tempo!</p>
            </div>
            <div class="comentario">
                <h3>Frances karla Viana cardoso</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Adorei o serviço.  Fiz em outros lugares e nada comparava. Delicados  e não tem excessos  de cola muito natural</p>
            </div>
            <div class="comentario">
                <h3>Patricia Sakugawa</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>A Keissy é uma ótima profissional , faz seu trabalho , com carinho e atenção, lugar calmo , limpo e agradável , super recomendo</p>
            </div>
            <div class="comentario">
                <h3>Manami Takano</h3>
                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                <p>Já estou fazendo meus cílios por 2 anos com ela! Super profissional com técnica japonesa!</p>
            </div>
        </div>

    </div>
</div>
</div>



</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.nivo",
    "tools/jquery.fancybox",
    "tools/contador-clientes",
    "tools/jquery.slick"
)); ?>

<script>
    $(function(){
        $(".counter").counterUp({
            delay: 100,
            time:7000
        });
    });
</script>

<script>
    $(function(){
        $(".autoplay").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000
        });
    });
</script>

<script>
    $(function(){
        $("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });

    $(function(){
     function scrollTopMenu(a){
         var id_element = $(a).attr("data-id");
         var top = $("#"+id_element).offset().top -150;
         if(id_element === "home")
         {
             var top = 0;
         }
         $("html, body").animate({
             scrollTop: top
         }, 700);
     }
     $("ul li a").click(function(){
         scrollTopMenu($(this));
     });
     $("a.click-second").click(function(){
         scrollTopMenu($(this));
     });
 });
</script>

</body>
</html>