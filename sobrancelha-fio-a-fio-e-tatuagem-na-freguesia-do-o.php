<?php
$title       = "Sobrancelha Fio a Fio e Tatuagem na Freguesia do Ó";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A vida útil de Sobrancelha Fio a Fio e Tatuagem na Freguesia do Ó é de cerca de 1 a 5 anos, dependendo do tipo de pele de cada pessoa e da técnica que você escolher usar. Essa técnica é indicada para mulheres com poucas sobrancelhas e muitas manchas, ou mulheres que gostam de ser bem definidas. Entre em contato conosco agora e marque um horário com a melhor empresa deste segmento, não perca tempo.</p>
<p>Por ser a principal empresa quando falamos de cilios, a Maxicilios se dispõe a adquirir os melhores e mais modernos recursos para atender seus clientes sempre da melhor forma. Possuindo como objetivo viabilizar tanto Estúdio de Sobrancelha de Rena, Sobrancelha Micropigmentada Preço, Extração de Cravos e Espinhas, Depilação Com Cera Cavada e Extensão de Cílios Preço, quanto como Sobrancelha Fio a Fio e Tatuagem na Freguesia do Ó mantendo a qualidade e a eficiência que você deseja. Entre em contato e faça uma cotação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>