<?php
$title       = "Manutenção de Cílios Fio a Fio no Bairro do Limão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A manutenção dos cílios fio a fio é uma técnica que separa os fios e os cola diretamente nos cílios a 0,2 cm da pele, para isso a cliente deve ter cílios saudáveis, nem muito jovens nem para o fim da vida. Para que você possa se alongar. Este procedimento não é adequado para crianças menores de 16 anos porque os fios não suportam o peso da extensão, entre em contato e marque seu horário com a melhor do mercado.</p>
<p>Com uma alta credibilidade no mercado de cilios, proporcionando com qualidade, viabilidade e custo x benefício em Manutenção de Cílios Fio a Fio no Bairro do Limão, a empresa Maxicilios vem crescendo e mostrando seu potencial através, também de Alongamento de Cílios Postiços Valor, Sobrancelha de Henna Definitiva, Extensão de Cílios Preço, Sobrancelha de Hena Preço e Alongamento de Cílios Efeito Natural, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>