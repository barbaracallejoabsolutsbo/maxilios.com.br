<?php
$title       = "Sobrancelha Fio a Fio Microblading na Vila Esperança";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na tecnologia de Sobrancelha Fio a Fio Microblading na Vila Esperança, os fios são desenhados na mesma espessura e direção que os pelos existentes na pele do cliente. Além disso, o pigmento é implantado na camada superficial da pele, tornando o resultado bastante natural. Esta técnica é indicada para quem tem sobrancelhas franzidas, podendo até trazer um efeito natural ao rosto, conte com a melhor para realizar esta técnica.</p>
<p>Buscando por uma empresa de credibilidade no segmento de cilios, para que, você que busca por Sobrancelha Fio a Fio Microblading na Vila Esperança, tenha a garantia de qualidade e idoneidade, contar com a Maxicilios é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Espaço Para Sobrancelhas, Extensão de Cílios Preço, Manutenção de Sobrancelha de Hena, Depilação Intima Feminina Valor e Depilação Com Cera Cavada para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>