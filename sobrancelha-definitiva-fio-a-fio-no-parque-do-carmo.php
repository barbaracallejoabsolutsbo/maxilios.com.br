<?php
$title       = "Sobrancelha Definitiva Fio a Fio no Parque do Carmo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na técnica de Sobrancelha Definitiva Fio a Fio no Parque do Carmo, a espessura e a direção dos pelos são iguais às que já existem na pele da cliente. Além disso, o pigmento é implantado na camada superficial da pele para deixar o resultado muito natural. Esta técnica é indicada para pessoas que possuem sobrancelhas franzidas, realizando este procedimento pode até trazer um efeito natural ao rosto da mulher que relaxar.</p>
<p>Além de sermos uma empresa especializada em Sobrancelha Definitiva Fio a Fio no Parque do Carmo disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Manutenção de Sobrancelha de Hena, Depilação Com Cera Cavada, Clínica de Massagem Relaxante, Limpeza de Pele Profissional Preço e Extensão de Cílios Preço. Com a ampla experiência que a equipe Maxicilios possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de cilios.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>