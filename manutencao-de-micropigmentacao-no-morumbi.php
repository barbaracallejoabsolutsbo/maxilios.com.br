<?php
$title       = "Manutenção de Micropigmentação no Morumbi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ao realizar a manutenção do micropigmentação as restrições são as mesmas da depilação ou tatuagem, ou seja, nos primeiros dias após o processamento da micropigmentação, evite a exposição ao sol, não arranhe a área e evite o uso de produtos que contenham ácido. Os clientes podem retomar as atividades diárias normais. Entre em contato com nossa equipe e saiba mais informações sobre este procedimento.</p>
<p>Com uma alta credibilidade no mercado de cilios, proporcionando com qualidade, viabilidade e custo x benefício em Manutenção de Micropigmentação no Morumbi, a empresa Maxicilios vem crescendo e mostrando seu potencial através, também de Tatuagem de Henna Para Sobrancelha, Extração de Cravos e Espinhas, Sobrancelha de Henna Definitiva, Depilação Com Cera Fria Preço e Cílios Efeito Natural Preço, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>