<?php
$title       = "Extensão de Cílios Volume Russo na Saúde";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O procedimento de Extensão de Cílios Volume Russo na Saúde  envolve a aplicação de fios sintéticos entre cílios reais para aumentar o volume e alongá-los. Embora o clássico "linha por linha" seja mais discreto e refinado, o procedimento russo é adequado para a categoria "drama" e fornece um visual mais atraente e completo que atrai a atenção das pessoas que passam, faça este procedimento com a Maxicilios.</p>
<p>Além de sermos uma empresa especializada em Extensão de Cílios Volume Russo na Saúde disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Alongamento de Cílios Postiços Valor, Depilação Com Cera Fria Preço, Clínica de Massagem Relaxante, Espaço Para Sobrancelhas e Limpeza de Pele Profunda Valor. Com a ampla experiência que a equipe Maxicilios possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de cilios.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>