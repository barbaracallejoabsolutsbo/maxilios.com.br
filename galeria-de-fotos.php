<?php

    $title       = "Galeria de Fotos";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "galeria-fotos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array($title)); ?>
            <h1><?php echo $h1;?></h1>
            <section>
                <h2>Galeria 1</h2>
                <?php echo $padrao->listaGaleria("Item", 4); ?>
            </section>
            <section>
                <h2>Galeria 2</h2>
                <?php echo $padrao->listaGaleria("Item", 4); ?>
            </section>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox"
    )); ?>
    
</body>
</html>