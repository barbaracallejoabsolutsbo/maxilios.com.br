<?php
$title       = "Sobrancelha de Henna Definitiva em Cidade Aracília - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As sobrancelhas de henna definitiva usam um corante, neste caso em particular, pode corrigir manchas e criar um desenho de contorno assimétrico nas sobrancelhas, você pode escolher a cor que será usada, não só existe preto no mercado de sobrancelhas, também tem marrom, tons de marrom claro e dourado escuro, dependendo do tom da sua pele e da cor do cabelo, faça esta técnica com a melhor.</p>
<p>Com credibilidade no mercado de cilios, a Maxicilios trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Depilação Com Cera Fria Preço, Alongamento de Cílios Fio A Fio, Sobrancelha Micropigmentada Preço, Tatuagem de Henna Para Sobrancelha e Sobrancelha de Hena Preço quanto em Sobrancelha de Henna Definitiva em Cidade Aracília - Guarulhos. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>