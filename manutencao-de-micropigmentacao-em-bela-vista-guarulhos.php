<?php
$title       = "Manutenção de Micropigmentação em Bela Vista - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As sobrancelhas trazem vitalidade e harmonia a todo o rosto e realçam o aspecto. Por isso, devem seguir o formato que melhor se adapte às suas expressões faciais. Para ter sobrancelhas bem desenhadas e naturais, nada melhor do que respeitar o contorno das sobrancelhas, portanto, a manutenção da micropigmentação deve ser realizada por um profissional altamente capacitado.</p>
<p>Você procura por Manutenção de Micropigmentação em Bela Vista - Guarulhos? Contar com empresas especializadas no segmento de cilios é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Maxicilios é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Alongamento de Cílios Efeito Natural, Sobrancelha Definitiva Fio a Fio, Extração de Cravos e Espinhas, Depilação Corpo Todo Valor e Tatuagem de Henna Para Sobrancelha e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>