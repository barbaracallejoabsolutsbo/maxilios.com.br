<?php
$title       = "Clínica de Massagem Relaxante em Campo Limpo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Maxicilios Clínica de Massagem Relaxante em Campo Limpo utiliza uma técnica com movimentos suaves e poderosos por todo o corpo para proporcionar relaxamento muscular e aliviar a tensão e o stress, também usamos óleos e cremes relacionados com a aroma terapia para massagens relaxantes para proporcionar uma saúde eficaz e uma sensação de conforto e tranquilidade, procure sempre a melhor para realizar este procedimento.</p>
<p>Por ser a principal empresa atuante no mercado de cilios, a Maxicilios conta com os melhores recursos visando fornecer não somente Sobrancelha Definitiva Fio a Fio, Manutenção de Micropigmentação, Micropigmentação de Sobrancelha Preço, Alongamento de Cílios Fio A Fio e Tatuagem de Henna Para Sobrancelha, mas também, Clínica de Massagem Relaxante em Campo Limpo com a qualidade e a eficiência que você tanto procura. Contate-nos e realize uma cotação com a nossa equipe especializada e equipada com as melhores ferramentas para melhor te atender.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>