<?php
$title       = "Manutenção de Cílios Fio a Fio no Tucuruvi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Vale ressaltar que antes de decidir estender e realizar as manutenções de cílios fio a fio, é importante estudar mais o local e os profissionais. Como os olhos são muito delicados, é necessário tomar alguns cuidados. Se você ainda tiver dúvidas sobre este procedimento, entre em contato com nossa equipe imediatamente, iremos tirar todas as suas dúvidas sobre esta manutenção que realizamos.</p>
<p>Nós da Maxicilios estamos entre as principais empresas qualificadas no ramo de cilios. Temos como principal missão realizar uma ótima assessoria tanto em Manutenção de Cílios Fio a Fio no Tucuruvi, quanto à Depilação Corpo Todo Valor, Sobrancelha Definitiva Fio a Fio, Cílios Postiço Naturais Preço, Manutenção de Cílios Russo e Manutenção de Cílios Fio a Fio, uma vez que, contamos com profissionais qualificados e prontos para realizarem um ótimo atendimento. Entre em contato conosco e tenha a satisfação que busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>