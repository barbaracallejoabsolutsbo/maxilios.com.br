<?php
$title       = "Alongamento de Cílios Fio A Fio em Taboão da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para as mulheres que querem olhar para cima, o alagamento de cílios fio a fio inclui a aplicação de fio sintético ou fio de seda nos cílios naturais de cada cliente. Os materiais e métodos de aplicação utilizados não prejudicam a saúde do fio natural. Entre em contato com nossa equipe e saiba mais sobre os procedimentos realizados, oferecemos o melhor procedimento com o melhor preço do mercado.</p>
<p>Além de sermos uma empresa especializada em Alongamento de Cílios Fio A Fio em Taboão da Serra disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Sobrancelha Fio a Fio Microblading, Cílios Efeito Natural Preço, Design de Sobrancelha Com Henna, Sobrancelha Definitiva Fio a Fio e Depilação Com Cera Cavada. Com a ampla experiência que a equipe Maxicilios possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de cilios.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>