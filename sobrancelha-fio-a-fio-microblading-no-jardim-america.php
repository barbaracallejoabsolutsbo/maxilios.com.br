<?php
$title       = "Sobrancelha Fio a Fio Microblading no Jardim América";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na realização da Sobrancelha Fio a Fio Microblading no Jardim América, as restrições são as mesmas para depilação ou tatuagem, ou seja, evitar a exposição ao sol de alguns dias atrás, não riscar a área e evitar o uso de produtos que contenham ácidos. Após realizar este procedimento, os clientes podem recuperar as atividades diárias normais. Entre em contato com nossa equipe e saiba mais sobre esta técnica que realizamos.</p>
<p>Nós da Maxicilios trabalhamos dia a dia para garantir o melhor em Sobrancelha Fio a Fio Microblading no Jardim América e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de cilios com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Alongamento de Cílios Volume Russo, Manutenção de Micropigmentação, Sobrancelha Micropigmentada Preço, Manutenção de Cílios Russo e Manutenção de Cílios Fio a Fio e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>