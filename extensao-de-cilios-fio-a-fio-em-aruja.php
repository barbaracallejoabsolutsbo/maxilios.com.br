<?php
$title       = "Extensão de Cílios Fio a Fio em Arujá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A técnica de cílios fio a fio é um procedimento que, em vez de agrupar, separe os fios de seda e cole-os diretamente nos cílios a 0,2 cm de distância da pele, as clientes devem ter cílios saudáveis, nem muito jovens nem em fim de vida, assim, este procedimento será realizado da forma correta Este procedimento não é adequado para crianças menores de 16 anos porque os fios não suportam o peso da extensão.</p>
<p>Você procura por Extensão de Cílios Fio a Fio em Arujá? Contar com empresas especializadas no segmento de cilios é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Maxicilios é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Depilação Com Cera Fria Preço, Espaço Para Sobrancelhas, Massagem Relaxante Preço, Alongamento de Cílios Efeito Natural e Depilação Corpo Todo Valor e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>