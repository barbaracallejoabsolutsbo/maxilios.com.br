<?php
$title       = "Manutenção de Cílios Russo em Itaim Bibi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Manutenção de Cílios Russo em Itaim Bibi envolve a aplicação de fios sintéticos entre os cílios reais para aumentar o volume dos cílios e alongá-los. Embora o clássico "linha por linha" seja mais discreto e refinado, esta técnica russa é adequada para a categoria "drama" e fornece um visual mais atraente e completo que atrai a atenção das pessoas que passam, faça este procedimento com a melhor do ramo.</p>
<p>A Maxicilios, como uma empresa em constante desenvolvimento quando se trata do mercado de cilios visa trazer o melhor resultado em Manutenção de Cílios Russo em Itaim Bibi para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Alongamento de Cílios Efeito Natural, Manutenção de Cílios Postiços, Design de Sobrancelha Com Henna, Depilação Corpo Todo Valor e Estúdio de Sobrancelha de Rena para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>