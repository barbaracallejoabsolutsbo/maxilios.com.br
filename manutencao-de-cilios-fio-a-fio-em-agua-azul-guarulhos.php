<?php
$title       = "Manutenção de Cílios Fio a Fio em Água Azul - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Vale ressaltar que antes de decidir estender e realizar as manutenções de cílios fio a fio, é importante estudar mais o local e os profissionais. Como os olhos são muito delicados, é necessário tomar alguns cuidados. Se você ainda tiver dúvidas sobre este procedimento, entre em contato com nossa equipe imediatamente, iremos tirar todas as suas dúvidas sobre esta manutenção que realizamos.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Manutenção de Cílios Fio a Fio em Água Azul - Guarulhos? A Maxicilios é o que você precisa! Sendo uma das principais empresas do ramo de cilios consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Sobrancelha Micropigmentada Preço, Limpeza de Pele Profissional Preço, Manutenção de Cílios Postiços, Manutenção de Cílios Fio a Fio e Alongamento de Cílios Preço. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>