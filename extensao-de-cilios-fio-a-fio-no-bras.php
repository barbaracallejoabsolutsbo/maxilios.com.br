<?php
$title       = "Extensão de Cílios Fio a Fio no Brás";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>É importante que sempre antes de realizar este procedimento é necessário pesquisar uma empresa que é renomada neste ramo, assim, você terá a garantia de uma Extensão de Cílios Fio a Fio no Brás de excelente qualidade, este procedimento requer muitos cuidados, sendo assim, procure sempre a Maxicilios, nossas profissionais são altamente qualificadas para realizar este procedimento com excelente qualidade.</p>
<p>Como uma empresa de confiança no mercado de cilios, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Extensão de Cílios Fio a Fio no Brás. A Maxicilios vem crescendo e mostrando seu potencial através de Depilação Com Cera Cavada, Clínica de Massagem Relaxante, Manutenção de Cílios Russo, Micropigmentação de Sobrancelhas Fio a Fio e limpeza peeling de diamante, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>