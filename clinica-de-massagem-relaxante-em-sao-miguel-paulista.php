<?php
$title       = "Clínica de Massagem Relaxante em São Miguel Paulista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Maxicilios Clínica de Massagem Relaxante em São Miguel Paulista utiliza uma técnica com movimentos suaves e poderosos por todo o corpo para proporcionar relaxamento muscular e aliviar a tensão e o stress, também usamos óleos e cremes relacionados com a aroma terapia para massagens relaxantes para proporcionar uma saúde eficaz e uma sensação de conforto e tranquilidade, procure sempre a melhor para realizar este procedimento.</p>
<p>Sendo uma das principais empresas do segmento de cilios, a Maxicilios possui os melhores recursos do mercado com o objetivo de disponibilizar Alongamento de Cílios Postiços Valor, Depilação Com Cera Cavada, Micropigmentação de Sobrancelhas Fio a Fio, Cílios Postiço Naturais Preço e Cílios Efeito Natural Preço com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Clínica de Massagem Relaxante em São Miguel Paulista com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>